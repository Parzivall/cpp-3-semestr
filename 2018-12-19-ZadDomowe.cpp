#include <iostream>

#define rozmiar 10

template <class T> int zlicz(T szukana, T tab[rozmiar]);

int main(){
	float szukanaF, tabF[rozmiar];
	double szukanaD, tabD[rozmiar];
	
	std::cout << "Wprowadz " << rozmiar << " liczb do tablicy 1 (float)" << std::endl;
	for(int i = 0; i < rozmiar; i++){
		std::cin >> tabF[i];
	}
	
	std::cout << "Wprowadz " << rozmiar << " liczb do tablicy 2 (double)" << std::endl;
	for(int i = 0; i < rozmiar; i++){
		std::cin >> tabD[i];
	}
	
	std::cout << "Jaka liczbe chcesz znalezc w tablicy 1 (typu float)?\t";
	std::cin >> szukanaF;
	
	std::cout << "Jaka liczbe chcesz znalezc w tablicy 2 (typu double)?\t";
	std::cin >> szukanaD;
	
	std::cout << "W tablicy 1 (float) liczba: \t'" << szukanaF << "'\t wystepuje: " << zlicz(szukanaF, tabF) << " razy." << std::endl;
	std::cout << "W tablicy 2 (double) liczba: \t'" << szukanaD << "'\t wystepuje: " << zlicz(szukanaD, tabD) << " razy." << std::endl;	
	return 0;
}

template<class T> int zlicz(T szukana, T tab[rozmiar]){
	int ile_razy = 0;
	for(int i = 0; i < rozmiar; i++){
		if(tab[i] == szukana){
			ile_razy += 1;
		}
	}
	return ile_razy;
}