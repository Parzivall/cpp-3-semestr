#include <iostream>

template<class T> T odejmij(T x, T y);

int main(){
	int x, y;
	float a, b;
	double c, d;
	
	std::cout << "Podaj 2 liczby (int):\t";
	std::cin >> x >> y; 
	
	std::cout << "Podaj 2 liczby (float):\t";
	std::cin >> a >> b;
	
	std::cout << "Podaj 2 liczby (double):\t";
	std::cin >> c >> d;  
	
	std::cout << "\n\t\tWYNIKI:" << std::endl;
	std::cout << x << " - " << y << " = " << odejmij(x, y) << std::endl;
	std::cout << a << " - " << b << " = " << odejmij(a, b) << std::endl;
	std::cout << c << " - " << d << " = " << odejmij(c, d) << std::endl;
	return 0;	
}

template<class T> T odejmij(T x, T y){
		return x-y;
}
