#include <iostream>

class publikacja{
	private:
		int rok_wydania;
	public:
		publikacja(int);
		void drukuj_pierwszy();
};

class ksiazka: public publikacja{
	private:
		int cena;
	public:
		ksiazka(int, int);
		void drukuj_drugi();
};

publikacja::publikacja(int rok){/*Ciało konstruktora parametrowego*/
	rok_wydania = rok;
}

ksiazka::ksiazka(int rok_drugi, int wartosc):publikacja(rok_drugi){/*ciało konstruktora dla klasy książka, który dodatkowo wywołuje konstruktor klasy publikacja*/
	cena = wartosc;
}

void publikacja::drukuj_pierwszy(){/*ciało metody drukującej 1 obiekt*/
	std::cout << "\nRok wydania ksiazki to:\t" << rok_wydania << "." << std::endl;
}

void ksiazka::drukuj_drugi(){/*ciało metody drukującej 2 obiekt*/
	drukuj_pierwszy();
	std::cout << "Cena ksiazki to:\t" << cena << "." << std::endl;
}

int main(){
	int rok, rok_drugi;
	int wartosc;
	
	std::cout << "Podaj rok wydania ksiazki:\t";
	std::cin >> rok;
	
	std::cout << "Podaj rok wydania drugiej ksiazki:\t";
	std::cin >> rok_drugi;
	
	std::cout << "Podaj cene ksiazki:\t";
	std::cin >> wartosc;
	
	publikacja obiekt1(rok);
	ksiazka obiekt2(rok_drugi, wartosc);
	
	std::cout <<"\n\t\tObiekt 1" << std::endl;
	obiekt1.drukuj_pierwszy();
	std::cout <<"\n\t\tObiekt 2" << std::endl;
	obiekt2.drukuj_drugi();
	
	return 0;
}