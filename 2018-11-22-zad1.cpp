#include <iostream>

class CzastkaEl{
	private:
		double masa;
	public:
		CzastkaEl(double);
		void drukuj();
		double zwrocMase();
};

class CzastkaElN: public CzastkaEl{
	private:
		float ladunekElektr;
	public:
		CzastkaElN(double, float);
		void drukuj2();
};

class NowaKlasa: public CzastkaElN{
	private:
		int liczba;
	public:
		NowaKlasa(double, float, int);
		void drukuj3();
};

CzastkaEl::CzastkaEl(double m){
	masa = m;
}

void CzastkaEl::drukuj(){
	std::cout << "masa czastki: " << masa << std::endl;
}

double CzastkaEl::zwrocMase(){
	return masa;
}

CzastkaElN::CzastkaElN(double m1, float lad2):CzastkaEl(m1),ladunekElektr(lad2){}

void CzastkaElN::drukuj2(){
	std::cout << "masa czastki: " << zwrocMase() << " ladunek elektryczny czastki: " << ladunekElektr << std::endl;
}
/*
NowaKlasa::(double m2, float lad3, int x): CzastkaElN(lad3), ladunekElektr(lad3) {
		liczba = x;
}*/

NowaKlasa::NowaKlasa(double m2, float lad2, int x):CzastkaElN(m2, lad2),liczba(x){}


void NowaKlasa::drukuj3(){
	std::cout << "\nLiczba: " << liczba << std::endl;
}

int main(){
	double m, m1, m2;
	float lad2, lad3;
	int x;
	
	std::cout << "Podaj m:\t";
	std::cin >> m;
	
	std::cout << "Podaj m1:\t";
	std::cin >> m1;
	
	std::cout << "Podaj m2:\t";
	std::cin >> m2;
	
	std::cout << "Podaj lad2:\t";
	std::cin >> lad2;

	std::cout << "Podaj lad3:\t";
	std::cin >> lad3;

	std::cout << "Podaj x:\t";
	std::cin >> x;
	
	CzastkaEl obiekt1(m);
	CzastkaElN obiekt2(m1, lad2);
	NowaKlasa obiekt3(m2, lad3, x);
	
	std::cout << "\n\t\tObiekt 1" << std::endl;
	obiekt1.drukuj();
	
	std::cout << "\n\t\tObiekt 2" << std::endl;
	obiekt2.drukuj2();
	
	std::cout << "\n\t\tObiekt 3" << std::endl;
	obiekt3.CzastkaElN::drukuj2();
	obiekt3.drukuj3();
	
return 0;
}