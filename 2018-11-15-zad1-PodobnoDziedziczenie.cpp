 #include <iostream>
 #include <cassert>
 #include <cstdlib>
 #include <cstring>

class Nazwa2{
	private:
		char * zawartosc;
		int dlugosc_napisu;
		static int ilosc_obiektow;
	
	public:
 		Nazwa2(char *);
 		Nazwa2();
 		~Nazwa2();
 
 	friend std::ostream & operator << (std::ostream &, const Nazwa2 &);
};

int Nazwa2::ilosc_obiektow = 0;

Nazwa2::Nazwa2(char * w){
	dlugosc_napisu = strlen(w);
	zawartosc = new char[dlugosc_napisu+1];
	assert(zawartosc);
	std::strcpy(zawartosc,w);
	std::cout << "Utworzono obiekt klasy Nazwa2" << std::endl;
	ilosc_obiektow++;
}

Nazwa2::Nazwa2(){
	dlugosc_napisu = 5;
	zawartosc = new char[dlugosc_napisu];
	assert(zawartosc);
	std::strcpy(zawartosc, "halo");
	ilosc_obiektow++;
	std::cout << "Utworzono obiekt domyslny klasy Nazwa2" << std::endl;
}
Nazwa2::~Nazwa2(){
	std::cout << zawartosc << " obiekt usuniety" << std::endl;
	delete [] zawartosc;
	--ilosc_obiektow;
	std::cout << "Pozostalo" << ilosc_obiektow << "obiektow" << std::endl;
}
 
std::ostream& operator << (std::ostream & wyjscie, const Nazwa2 & staly){
	wyjscie << staly.zawartosc;
	return wyjscie;
} 
 
void problem_z_alokacja(){
	std::cerr << "Alokacja nie powiodla się." << std::endl;
	exit(1);
}
 
void obiekt_ref(Nazwa2 & ref){
	std::cout << "Przekazano obiekt przez referencje: " << std::endl << ref ;
}

void obiekt_wartosc(Nazwa2 war){
	std::cout << "Przekazano obiekt przez wartosc: " << std::endl << war;
}
 
int main( )
{
	Nazwa2 pierwszy("poniedziałek");
	Nazwa2 drugi("wtorek");
	Nazwa2 trzeci("sroda");
	
	std::cout << "teraz zawartosci obiektow" << std::endl;
	std::cout << "obiekt pierwszy: " << pierwszy << std::endl;
	std::cout << "obiekt drugi: " << drugi << std::endl;
	std::cout << "obiekt trzeci: " << trzeci << std::endl;
	std::cout << "Teraz wywolujemy funkcje pobierajace obiekty " << "i wypisujace ich zawartosci" << std::endl;
	
	obiekt_ref(pierwszy);
	obiekt_wartosc(drugi);
	
	std::cout << "Teraz inicjalizujemy obiekt, innym obiektem." << std::endl;
	
	Nazwa2 czwarty = trzeci;
	std::cout << "obiekt czwarty: " << czwarty << std::endl;
	
	std::cout << "przypisanie obiektu pierwszy do obiektu piaty" << std::endl;
	
	Nazwa2 piaty;
	piaty = pierwszy;
	std::cout << "obiekt piaty: " << piaty << std::endl;
	
	return 0;
}