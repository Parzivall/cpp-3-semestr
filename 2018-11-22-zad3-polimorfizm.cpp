#include <iostream>

class A{
	private:
		int m;
	public:
		A(int);
		virtual void drukuj();
		int zwroc_m();
};

A::A(int x){
	m = x;
}

int A::zwroc_m(){
	return m;
}

void A::drukuj(){
	std::cout << "A= " << m << std::endl;
}

class B: public A{
	private:
		char c;
	public:
		B(int, char);
		void drukuj();
};

B::B(int x1, char ciag): A(x1){
	c = ciag;
}

void B::drukuj(){
	drukuj();
	std::cout << "Char= " << c << std::endl;
}

int main(){
	A *obiekt1 = mew ;
	B obiekt2(10, 'J');
	
	A *w = &obiekt1;
	w->drukuj();
	
	B *w2 = &obiekt2;
	w2->drukuj(); 
	
	/*
	std::cout << "\n\t\tObiekt 1" << std::endl;
	obiekt1.drukuj();
	
	std::cout << "\n\t\tObiekt 2" << std::endl;
	obiekt2.drukuj2();*/
	
	/*
		polimorfizm:
		1. dziedziczenie;
		2. przesłonięcie metody w klasie pochodnej metodą o tej samej nazwie
		3. metoda która ma być wywołana musi być od słowa virtual w klasie bazowej
		4. użycie wskaźników
	*/
	
	
	return 0;
}