#include <iostream>

class zespolone{
	private:
		int re, im;
	
	public:
		zespolone(int, int);
		zespolone();
		zespolone operator+(zespolone );
		zespolone operator-(zespolone );
		void drukuj();	
		
		friend std::ostream& operator<<(std::ostream&, zespolone);
		friend std::istream& operator>>(std::istream&, zespolone);
};

zespolone::zespolone(int m, int n){
	re = m;
	im = n;	
}

zespolone::zespolone(){
	re = 0;
	im = 0;	
}

zespolone zespolone::operator+(zespolone z0){
	int rz, ur;
	rz = re + z0.re;
	ur = im + z0.im;
	return zespolone(rz, ur);	
}

zespolone zespolone::operator-(zespolone z0){
	int rz, ur;
	rz = re - z0.re;
	ur = im - z0.im;
	return zespolone(rz, ur);	
}


void zespolone::drukuj(){
	std::cout << "Liczba rzeczywista:\t" << re << "\nLiczba Urojona:\t" << im << std::endl;	
}

std::ostream & operator<<(std::ostream& wyjscie, zespolone z1){
	wyjscie<< "(" << z1.re << "," << z1.im << ")" << std::endl;
	return wyjscie;	
}

std::istream & operator>>(std::istream& wejscie, zespolone z1){	
	std::cout << "Podaj czesc rzeczywista:\t";
	wejscie >>z1.re;
	std::cout << "Podaj czesc urojona:\t";
	wejscie >>z1.im;
	return wejscie;
}


int main(){
/*	int rzeczywista, urojona;
	std::cout << "Podaj czesc rzeczywista:\t";
	std::cin >> rzeczywista;
	
	std::cout << "Podaj czesc urojona:\t";
	std::cin >> urojona;
	
	zespolone z1(rzeczywista, urojona);*/
	zespolone z1;
	std::cin >> z1;
	zespolone z2(1, 2);
	zespolone z3(0, 0);
	
	std::cout << "\n\t\tZ1" << std::endl;
	z1.drukuj();
	
	z3 = z1.operator+(z2);
	std::cout << "\n z1 + z2 " << std::endl;
	std::cout << z3;
	
	z3 = z1.operator-(z2);
	std::cout << "\n z1 - z2 " << std::endl;
	std::cout << z3;
	
	return 0;	
}