#include <iostream>

class A{
	private:
		char c;
	public:
		A(char);
		virtual void drukuj();
		char zwroc_c();
};

A::A(char litera){
	c = litera;	
}

char A::zwroc_c(){
	return c;	
}

void A::drukuj(){
	std::cout << "Char=\t" << c << std::endl;	
}

class B: public A{
	private:
		int n;
	public:
		B(int, char);
		void drukuj();	
};

B::B(int x, char litera2): A(litera2){
	n = x;	
}

void B::drukuj(){
	A::drukuj();
	std::cout << "Int=\t" << n << std::endl;	
}

int main(){
	char litera, litera1;
	int x;
		
	std::cout << "Podaj litere:\t";
	std::cin >> litera;
	
	std::cout << "\n\t---OBIEKT 1---" << std::endl;
	A *w;
	
	A obiekt1(litera);
	w = &obiekt1;
	w->drukuj();
	
	
	std::cout << "\nPodaj litere1:\t";
	std::cin >> litera1;
	std::cout << "Podaj x:\t";
	std::cin >> x;
	
	std::cout << "\n\t---OBIEKT 2---" << std::endl;
	B obiekt2(x, litera1);
	
	w = &obiekt2;
	w->drukuj();
	
	return 0;	
}