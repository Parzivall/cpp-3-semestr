#include <iostream>

class A{
	private:
		int m;
	public:
		A(int);
		void drukuj();
		int zwroc_m();
};

A::A(int x){
	m = x;
}

int A::zwroc_m(){
	return m;
}

void A::drukuj(){
	std::cout << "A= " << m << std::endl;
}

class B: public A{
	private:
		char c;
	public:
		B(int, char);
		void drukuj2();
		char zwroc_c();
};

B::B(int x1, char ciag): A(x1){
	c = ciag;
}

void B::drukuj2(){
	drukuj();
	std::cout << "Ciag= " << c << std::endl;
}


char B::zwroc_c(){
	return c;	
}

class C: public B{
	private:
		int tab[5];
	public:
		C(int, char, int[]);
		void drukuj3();
};

C::C(int x2, char ciag2, int tablica[5]):B(x2, ciag2){
	for(int i = 0; i < 5; i++)
		tab[i] = tablica[i];
}

void C::drukuj3(){
	drukuj2();
	for(int i = 0; i < 5; i++)
		std::cout << "tab[" << i << "]=\t" << tab[i] << std::endl;
}

int main(){
	int x, x1, x2, tablica[5] = {1, 2, 3, 4, 5};
	char ciag, ciag2;
	
	
	
	return 0;
}