#include <iostream>

template <class T>
class klasa{
	private:
		T zmienna;
	
	public:
		klasa(T);
		void drukuj();
};

template <class T>
klasa<T>::klasa(T x){
	zmienna = x;
}

template <class T>
void klasa<T>::drukuj(){
	std::cout << "Zmienna:\t" << zmienna << std::endl;	
}


int main(){
	klasa <char> obiekt('a');
	obiekt.drukuj();
	
	return 0;	
}