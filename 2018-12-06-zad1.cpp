#include <iostream>
#include <cstring>
#include <cassert>

class srodekTransportu{
	private:
		char *model;
	
	public:
		srodekTransportu(char[50]);
		virtual void drukuj();
		char * zwrocModel();
		~srodekTransportu();
};

srodekTransportu::srodekTransportu(char tab1[50]){
	model = new char[51];
	assert(model);
	strcpy(model, tab1);	
}

char * srodekTransportu::zwrocModel(){
	return model;
}

void srodekTransportu::drukuj(){
	std::cout << "Model:\t" << model << std::endl;	
}

srodekTransportu::~srodekTransportu(){
	delete []model;
	std::cout << "\nZwolniono pamiec dla modelu" << std::endl;
}


class samochod: public srodekTransportu{
	private:
		char *typPaliwa;
	public:
		samochod(char[50], char[50]);
		~samochod();
		void drukuj();	
};

samochod::samochod(char tab1[50], char tab2[50]): srodekTransportu(tab1){
	typPaliwa = new char[51];
	assert(typPaliwa);
	strcpy(typPaliwa, tab2); 
}

samochod::~samochod(){	
	delete []typPaliwa;
	std::cout << "\nZwolniono pamiec dla typu paliwa" << std::endl;	
}

void samochod::drukuj(){
	std::cout << "Model:\t" << zwrocModel() << "\nTyp paliwa:\t" << typPaliwa << std::endl;	
}

int main(){
	char tab1[50], tab1_1[50] ,tab2[50];
	std::cout << "Podaj model 1 auta:\t";
	std::cin >> tab1;
	std::cout << "Podaj typ paliwa auta:\t";
	std::cin >> tab2;
	
	std::cout << "\n\t\tObiekt 1" << std::endl;
	srodekTransportu obiekt1(tab1);
	srodekTransportu *w;
	
	w = &obiekt1;
	w->drukuj();
	
	std::cout << "\n\t\tObiekt 2" << std::endl;
	samochod obiekt2(tab1, tab2);
	w = &obiekt2;
	w->drukuj();
	
	
	return 0;	
}