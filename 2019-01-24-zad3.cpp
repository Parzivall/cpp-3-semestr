#include <iostream>

class A{
	private:
		char c;
	public:
		A(char);
		virtual void drukuj();	
		char zwroc_c();
};

class B:public A{
	private:
		int n;
	public:
		B(char, int);
		void drukuj();	
};

A::A(char z){
	c = z;
}

void A::drukuj(){
	std::cout << "obiekt z klasy A:\t" << c << std::endl;
}

char A::zwroc_c(){
	return c;	
}

B::B(char z1, int x):A(z1){
	n = x;	
}

void B::drukuj(){
	std::cout << "Obiekt z klasy A:\t" << zwroc_c() << "\nObiekt z klasy B:\t" << n << std::endl;	
}

int main(){
	A obiekt1('z');
	B obiekt2('d', 5);
	A *w = &obiekt1;
	std::cout << "\nObiekt1" << std::endl;
	w->drukuj();
	std::cout << "\nObiekt2" << std::endl;
	w = &obiekt2;
	w->drukuj();
	
	return 0;	
}
