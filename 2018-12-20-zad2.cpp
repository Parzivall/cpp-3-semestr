#include <iostream>
#include <cstdlib>
#include <string>

class Zamek{
	private:
		char *nazwa;
		
	public:
		static Zamek * wskaznik_siedziby;
		Zamek(char *);
		void opisz();
		static void gdzie_Krol();		
};

Zamek* Zamek::wskaznik_siedziby;

Zamek::Zamek(char * n): nazwa(n){}

void Zamek::opisz(){
	std::cout << "Tutaj " << nazwa << std::endl;	
}

void Zamek::gdzie_Krol(){
	std::cout << "Komunikat od funckji statycznej: " << "Krol jest teraz w zamku: " << wskaznik_siedziby->nazwa;	
}


//teraz szablon (wzorzec) klas ze składowymi statycznymi

template<class typ>
class castle{
	private:
		char *nazwa;
	
	public:
		static castle<typ> * wskaznik_castle;
		static int calkowity;
		static typ skladnik_t;
		castle(char *);
		void opisz();
		static void gdzie_stolica();	
};

template<class typ>
	castle<typ> * castle<typ>::wskaznik_castle;

template<class typ>
	int castle<typ>::calkowity;

template<class typ>
	typ castle<typ>::skladnik_t;
		
template<class typ>
	castle<typ>::castle(char *n): nazwa(n){}
	
template<class typ>
void castle<typ>::opisz(){
	std::cout << "Tutaj: " << nazwa << std::endl;	
}


template<class typ>
void castle<typ>::gdzie_stolica(){
	std::cout << "Komunikat od funkcji statycznej: " << "stolica: " << wskaznik_castle->nazwa;	
}

int main()
{
	Zamek zamekWawelski("Wawel");
	Zamek zamekNiepolomicki("Niepolomice");
	
	Zamek::wskaznik_siedziby = &zamekWawelski;
	
	zamekWawelski.opisz();
	
	zamekNiepolomicki.opisz();
	
	
	Zamek::gdzie_Krol();
	std::cout << "Krol wyjechal do Niepolomic" << std::endl;
	
	zamekWawelski.wskaznik_siedziby = &zamekNiepolomicki;
	zamekWawelski.opisz();
	zamekNiepolomicki.opisz();
	
	Zamek::gdzie_Krol();
	
	castle<char> zamekWindsor("Windsor");
	castle<char> zamekTower("Tower");
	castle<char>::calkowity = 50;
	castle<char>::skladnik_t = 'y';
	castle<char>::wskaznik_castle = &zamekTower;
	
	zamekWindsor.opisz();
	zamekTower.opisz();
	castle<char>::gdzie_stolica();
	
	
	castle<int> zamekKyoto("Kyoto");
	castle<int> zamekTokio("Tokio");
	castle<int>::calkowity = 4;
	castle<int>::skladnik_t = 'h';
	castle<int>::wskaznik_castle = &zamekKyoto;
	
	zamekKyoto.opisz();
	zamekTokio.opisz();
	castle<int>::gdzie_stolica();
	
	return 0;
}